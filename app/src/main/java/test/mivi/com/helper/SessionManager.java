
package test.mivi.com.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;


public class SessionManager {

    // LogCat tag
    private static String TAG = SessionManager.class.getSimpleName();

    // Shared Preferences
    SharedPreferences pref;

    SharedPreferences.Editor editor;
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;
    private static SessionManager sessionManager;

    // Shared preferences file name
    private static final String PREF_NAME = "MIVI_TEST";

    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }
    public static SessionManager getInstance(Context ctx)
    {
        if(sessionManager == null)
        {
            sessionManager =  new SessionManager(ctx);
        }
        return  sessionManager;
    }

    //KEY Declared
    public static final String KEY_isLoggedIn ="isLoggedIn";


    public Boolean isLoggedIn(){
        return pref.getBoolean(KEY_isLoggedIn, false);
    }

    public void set_LoggedIn(Boolean loggedInStat){
        editor.putBoolean(KEY_isLoggedIn, loggedInStat);
        editor.commit();
    }

}


package test.mivi.com.helper;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;

import test.mivi.com.R;
import test.mivi.com.model.AccountsModel;
import test.mivi.com.model.ProductModel;
import test.mivi.com.model.ServicesModel;
import test.mivi.com.model.SubcriptionsModel;

public class UtilClass {

    public static JSONObject collectedData(Context c){
        String jsonString = "{}";
        JSONObject jobj = null;
        InputStream is = c.getResources().openRawResource(R.raw.collection);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
            jsonString = writer.toString();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            jobj = new JSONObject(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jobj;

    }


    public static AccountsModel getCustomerInfo(JSONObject jobj){
        AccountsModel accountObj = new AccountsModel();
        try {
            JSONObject accountinfo = jobj.getJSONObject("data");
            JSONObject attributesobj = accountinfo.getJSONObject("attributes");

            accountObj.setAccount_id(accountinfo.getInt("id"));

            accountObj.setContact_number(attributesobj.getString("contact-number"));
            accountObj.setDate_of_birth(attributesobj.getString("date-of-birth"));
            accountObj.setEmail_address(attributesobj.getString("email-address"));
            accountObj.setFirst_name(attributesobj.getString("first-name"));
            accountObj.setLast_name(attributesobj.getString("last-name"));
            accountObj.setNameTitle(attributesobj.getString("title"));
            accountObj.setNext_billing_date(attributesobj.getString("next-billing-date"));
            accountObj.setPayment_type(attributesobj.getString("payment-type"));
            accountObj.setUnbilled_charges(attributesobj.getString("unbilled-charges"));
            accountObj.setEmail_address_verified(attributesobj.getBoolean("email-address-verified"));
            accountObj.setEmail_subscription_status(attributesobj.getBoolean("email-subscription-status"));


            Log.d("creation",attributesobj.getString("payment-type"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return accountObj;
    }

    public static ArrayList<ServicesModel> getservicesInfo(JSONObject jobj){

        ArrayList<ServicesModel> serviceModelArrayobj = new ArrayList<>();

        try {
            JSONArray included_array = jobj.getJSONArray("included");
            for (int i=0;i<included_array.length();i++){
                JSONObject innerLoopObj = included_array.getJSONObject(i);

                String infoType = innerLoopObj.getString("type");

                if(infoType.equalsIgnoreCase("services")){

                    ServicesModel servicesInfoObj = new ServicesModel();
                    JSONObject attributesobj = innerLoopObj.getJSONObject("attributes");
                    servicesInfoObj.setServiceid(innerLoopObj.getString("id"));
                    servicesInfoObj.setCredit(attributesobj.getString("credit"));
                    servicesInfoObj.setCredit_expiry(attributesobj.getString("credit-expiry"));
                    servicesInfoObj.setData_usage_threshold(attributesobj.getBoolean("data-usage-threshold"));

                    JSONObject relationshipsobj = innerLoopObj.getJSONObject("relationships");
                    JSONObject relationshipssubcriptionobj = relationshipsobj.getJSONObject("subscriptions");
                    JSONArray serviceSubcriptionRelation = relationshipssubcriptionobj.getJSONArray("data");
                    servicesInfoObj.setSubcriptionsModelobj(getSubcriptionModel(serviceSubcriptionRelation,included_array));

                    serviceModelArrayobj.add(servicesInfoObj);

                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return serviceModelArrayobj;
    }

    public static ArrayList<SubcriptionsModel> getSubcriptionModel(JSONArray relation,JSONArray mainframe){

        ArrayList<SubcriptionsModel> subcriptionsModelArrayobj = new ArrayList<>();
        try {
            for (int i=0;i<mainframe.length();i++) {
                JSONObject innerLoopObj = mainframe.getJSONObject(i);
                String infoType = innerLoopObj.getString("type");
                if (infoType.equalsIgnoreCase("subscriptions")) {

                    String subscripId = innerLoopObj.getString("id");
                    boolean hasSubcription = false;

                    if(relation.length()>0){

                        for (int k=0;k<relation.length();k++){
                            JSONObject subsIdmatch = relation.getJSONObject(k);
                            if(subsIdmatch.getString("type").equalsIgnoreCase("subscriptions")) {
                                if(subsIdmatch.getString("id").equalsIgnoreCase(subscripId)){
                                    hasSubcription = true;
                                }
                            }
                        }
                    }

                    if(hasSubcription){

                        SubcriptionsModel subcriptionInfoObj = new SubcriptionsModel();
                        JSONObject attributesobj = innerLoopObj.getJSONObject("attributes");

                        subcriptionInfoObj.setSubcription_id(subscripId);

                        subcriptionInfoObj.setIncluded_data_balance(attributesobj.getString("included-data-balance"));
                        subcriptionInfoObj.setIncluded_credit_balance(attributesobj.getString("included-credit-balance"));
                        subcriptionInfoObj.setIncluded_rollover_credit_balance(attributesobj.getString("included-rollover-credit-balance"));
                        subcriptionInfoObj.setIncluded_rollover_data_balance(attributesobj.getString("included-rollover-data-balance"));
                        subcriptionInfoObj.setIncluded_international_talk_balance(attributesobj.getString("included-international-talk-balance"));
                        subcriptionInfoObj.setExpiry_date(attributesobj.getString("expiry-date"));
                        subcriptionInfoObj.setAuto_renewal(attributesobj.getBoolean("auto-renewal"));
                        subcriptionInfoObj.setPrimary_subscription(attributesobj.getBoolean("primary-subscription"));

                        JSONObject relationshipsobj = innerLoopObj.getJSONObject("relationships");
                        JSONObject relationshipsproductobj = relationshipsobj.getJSONObject("product");

                        //JSONArray subcriptionProductRelation = relationshipsproductobj.getJSONArray("data");

                        JSONObject subcriptionProductObj = relationshipsproductobj.getJSONObject("data");

                        subcriptionInfoObj.setProductModelobj(getProductModel(subcriptionProductObj,mainframe));

                        subcriptionsModelArrayobj.add(subcriptionInfoObj);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return subcriptionsModelArrayobj;
    }

    public static ArrayList<ProductModel> getProductModel(JSONObject relation,JSONArray mainframe){
        ArrayList<ProductModel> productModelArrayobj = new ArrayList<>();
        try {
            for (int i = 0; i < mainframe.length(); i++) {
                JSONObject innerLoopObj = mainframe.getJSONObject(i);
                String infoType = innerLoopObj.getString("type");
                if (infoType.equalsIgnoreCase("products")) {

                    String prodId = innerLoopObj.getString("id");
                    boolean hasProducts = false;

                    if(relation.getString("type").equalsIgnoreCase("products")) {
                        if(relation.getString("id").equalsIgnoreCase(prodId)){
                            hasProducts = true;
                        }
                    }

                    if(hasProducts) {
                        ProductModel productInfoObj = new ProductModel();

                        JSONObject attributesobj = innerLoopObj.getJSONObject("attributes");

                        productInfoObj.setProduct_id(prodId);

                        productInfoObj.setProduct_name(attributesobj.getString("name"));
                        productInfoObj.setIncluded_data(attributesobj.getString("included-data"));
                        productInfoObj.setIncluded_credit(attributesobj.getString("included-credit"));
                        productInfoObj.setIncluded_international_talk(attributesobj.getString("included-international-talk"));
                        productInfoObj.setUnlimited_text(attributesobj.getString("unlimited-text"));
                        productInfoObj.setUnlimited_talk(attributesobj.getString("unlimited-talk"));
                        productInfoObj.setUnlimited_international_talk(attributesobj.getBoolean("unlimited-international-talk"));
                        productInfoObj.setUnlimited_international_text(attributesobj.getBoolean("unlimited-international-text"));
                        productInfoObj.setProduct_price(attributesobj.getString("price"));

                        productModelArrayobj.add(productInfoObj);
                    }
                }
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }

        return productModelArrayobj;
    }

    /*public static ArrayList<ProductModel> getProductModel(JSONArray relation,JSONArray mainframe){
        ArrayList<ProductModel> productModelArrayobj = new ArrayList<>();
        try {
            for (int i = 0; i < mainframe.length(); i++) {
                JSONObject innerLoopObj = mainframe.getJSONObject(i);
                String infoType = innerLoopObj.getString("type");
                if (infoType.equalsIgnoreCase("products")) {

                    String prodId = innerLoopObj.getString("id");
                    boolean hasProducts = false;
                    if(relation.length()>0){

                        for (int k=0;k<relation.length();k++){
                            JSONObject prodIdmatch = relation.getJSONObject(k);
                            if(prodIdmatch.getString("type").equalsIgnoreCase("products")) {
                                if(prodIdmatch.getString("id").equalsIgnoreCase(prodId)){
                                    hasProducts = true;
                                }
                            }
                        }
                    }

                    if(hasProducts) {
                        ProductModel productInfoObj = new ProductModel();

                        JSONObject attributesobj = innerLoopObj.getJSONObject("attributes");

                        productInfoObj.setProduct_id(prodId);

                        productInfoObj.setProduct_name(attributesobj.getString("name"));
                        productInfoObj.setIncluded_data(attributesobj.getString("included-data"));
                        productInfoObj.setIncluded_credit(attributesobj.getString("included-credit"));
                        productInfoObj.setIncluded_international_talk(attributesobj.getString("included-international-talk"));
                        productInfoObj.setUnlimited_text(attributesobj.getString("unlimited-text"));
                        productInfoObj.setUnlimited_talk(attributesobj.getString("unlimited-talk"));
                        productInfoObj.setUnlimited_international_talk(attributesobj.getBoolean("unlimited-international-talk"));
                        productInfoObj.setUnlimited_international_text(attributesobj.getBoolean("unlimited-international-text"));
                        productInfoObj.setProduct_price(attributesobj.getString("price"));

                        productModelArrayobj.add(productInfoObj);
                    }
                }
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }

        return productModelArrayobj;
    }*/

}

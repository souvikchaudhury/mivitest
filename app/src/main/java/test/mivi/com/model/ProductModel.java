package test.mivi.com.model;

public class ProductModel {

    private String product_id,product_price;
    private String product_name,included_data,included_credit,included_international_talk,unlimited_text;
    private String unlimited_talk;
    private boolean unlimited_international_text,unlimited_international_talk;

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_price() {
        return product_price;
    }

    public void setProduct_price(String product_price) {
        this.product_price = product_price;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getIncluded_data() {
        return included_data;
    }

    public void setIncluded_data(String included_data) {
        this.included_data = included_data;
    }

    public String getIncluded_credit() {
        return included_credit;
    }

    public void setIncluded_credit(String included_credit) {
        this.included_credit = included_credit;
    }

    public String getIncluded_international_talk() {
        return included_international_talk;
    }

    public void setIncluded_international_talk(String included_international_talk) {
        this.included_international_talk = included_international_talk;
    }

    public String getUnlimited_text() {
        return unlimited_text;
    }

    public void setUnlimited_text(String unlimited_text) {
        this.unlimited_text = unlimited_text;
    }

    public String getUnlimited_talk() {
        return unlimited_talk;
    }

    public void setUnlimited_talk(String unlimited_talk) {
        this.unlimited_talk = unlimited_talk;
    }

    public boolean isUnlimited_international_text() {
        return unlimited_international_text;
    }

    public void setUnlimited_international_text(boolean unlimited_international_text) {
        this.unlimited_international_text = unlimited_international_text;
    }

    public boolean isUnlimited_international_talk() {
        return unlimited_international_talk;
    }

    public void setUnlimited_international_talk(boolean unlimited_international_talk) {
        this.unlimited_international_talk = unlimited_international_talk;
    }
}

package test.mivi.com.model;

public class AccountsModel {

    private int account_id;
    private String payment_type, unbilled_charges, next_billing_date, name_title, first_name,last_name;
    private String date_of_birth,contact_number,email_address;
    private boolean email_address_verified,email_subscription_status;


    public int getAccount_id() {
        return account_id;
    }

    public void setAccount_id(int account_id) {
        this.account_id = account_id;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getUnbilled_charges() {
        return unbilled_charges;
    }

    public void setUnbilled_charges(String unbilled_charges) {
        this.unbilled_charges = unbilled_charges;
    }

    public String getNext_billing_date() {
        return next_billing_date;
    }

    public void setNext_billing_date(String next_billing_date) {
        this.next_billing_date = next_billing_date;
    }

    public String getNameTitle() {
        return name_title;
    }

    public void setNameTitle(String title) {
        this.name_title = title;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(String date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getContact_number() {
        return contact_number;
    }

    public void setContact_number(String contact_number) {
        this.contact_number = contact_number;
    }

    public String getEmail_address() {
        return email_address;
    }

    public void setEmail_address(String email_address) {
        this.email_address = email_address;
    }

    public boolean isEmail_address_verified() {
        return email_address_verified;
    }

    public void setEmail_address_verified(boolean email_address_verified) {
        this.email_address_verified = email_address_verified;
    }

    public boolean isEmail_subscription_status() {
        return email_subscription_status;
    }

    public void setEmail_subscription_status(boolean email_subscription_status) {
        this.email_subscription_status = email_subscription_status;
    }
}

package test.mivi.com.model;

import java.util.ArrayList;

public class ServicesModel {

    private String serviceid,credit;
    private String credit_expiry;
    private boolean data_usage_threshold;

    private ArrayList<SubcriptionsModel> subcriptionsModelobj;


    public String getServiceid() {
        return serviceid;
    }

    public void setServiceid(String serviceid) {
        this.serviceid = serviceid;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public String getCredit_expiry() {
        return credit_expiry;
    }

    public void setCredit_expiry(String credit_expiry) {
        this.credit_expiry = credit_expiry;
    }

    public boolean isData_usage_threshold() {
        return data_usage_threshold;
    }

    public void setData_usage_threshold(boolean data_usage_threshold) {
        this.data_usage_threshold = data_usage_threshold;
    }

    public ArrayList<SubcriptionsModel> getSubcriptionsModelobj() {
        return subcriptionsModelobj;
    }

    public void setSubcriptionsModelobj(ArrayList<SubcriptionsModel> subcriptionsModelobj) {
        this.subcriptionsModelobj = subcriptionsModelobj;
    }


}

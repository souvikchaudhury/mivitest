package test.mivi.com.model;

import java.util.ArrayList;

public class SubcriptionsModel {

    private String subcription_id;
    private String included_data_balance,included_credit_balance,included_rollover_credit_balance;
    private String included_rollover_data_balance,included_international_talk_balance,expiry_date;
    private boolean auto_renewal,primary_subscription;

    private ArrayList<ProductModel> productModelobj;

    public String getSubcription_id() {
        return subcription_id;
    }

    public void setSubcription_id(String subcription_id) {
        this.subcription_id = subcription_id;
    }

    public String getIncluded_data_balance() {
        return included_data_balance;
    }

    public void setIncluded_data_balance(String included_data_balance) {
        this.included_data_balance = included_data_balance;
    }

    public String getIncluded_credit_balance() {
        return included_credit_balance;
    }

    public void setIncluded_credit_balance(String included_credit_balance) {
        this.included_credit_balance = included_credit_balance;
    }

    public String getIncluded_rollover_credit_balance() {
        return included_rollover_credit_balance;
    }

    public void setIncluded_rollover_credit_balance(String included_rollover_credit_balance) {
        this.included_rollover_credit_balance = included_rollover_credit_balance;
    }

    public String getIncluded_rollover_data_balance() {
        return included_rollover_data_balance;
    }

    public void setIncluded_rollover_data_balance(String included_rollover_data_balance) {
        this.included_rollover_data_balance = included_rollover_data_balance;
    }

    public String getIncluded_international_talk_balance() {
        return included_international_talk_balance;
    }

    public void setIncluded_international_talk_balance(String included_international_talk_balance) {
        this.included_international_talk_balance = included_international_talk_balance;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public void setExpiry_date(String expiry_date) {
        this.expiry_date = expiry_date;
    }

    public boolean isAuto_renewal() {
        return auto_renewal;
    }

    public void setAuto_renewal(boolean auto_renewal) {
        this.auto_renewal = auto_renewal;
    }

    public boolean isPrimary_subscription() {
        return primary_subscription;
    }

    public void setPrimary_subscription(boolean primary_subscription) {
        this.primary_subscription = primary_subscription;
    }

    public ArrayList<ProductModel> getProductModelobj() {
        return productModelobj;
    }

    public void setProductModelobj(ArrayList<ProductModel> productModelobj) {
        this.productModelobj = productModelobj;
    }
}

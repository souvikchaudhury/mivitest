package test.mivi.com.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

import butterknife.BindView;
import butterknife.ButterKnife;
import test.mivi.com.R;
import test.mivi.com.helper.SessionManager;
import test.mivi.com.model.AccountsModel;

import static test.mivi.com.helper.UtilClass.collectedData;
import static test.mivi.com.helper.UtilClass.getCustomerInfo;


public class LoginActivity extends AppCompatActivity {

    String jsonString = "";
    Context activityName;
    AccountsModel accountInfoObj;

    @BindView(R.id.loginbutton)Button loginbutton;
    @BindView(R.id.password)EditText password;
    @BindView(R.id.contactno)EditText contactno;
    @BindView(R.id.errortext)TextView errortext;


    SessionManager sessionmanagerObj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //getSupportActionBar().setTitle(R.string.action_sign_in_short);
        activityName = LoginActivity.this;
        ButterKnife.bind(this);
        sessionmanagerObj = new SessionManager(getApplicationContext());

        if(sessionmanagerObj.isLoggedIn()){
            launchHomeActivity();
        }

        JSONObject collectedJsonData = collectedData(activityName);
        accountInfoObj = getCustomerInfo(collectedJsonData);

        loginbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String contactinfo = contactno.getText().toString().trim();
                String passcode = password.getText().toString().trim();
                if(!contactinfo.isEmpty() && !passcode.isEmpty()){
                    if(contactinfo.equalsIgnoreCase(accountInfoObj.getContact_number()) && passcode.equalsIgnoreCase("12345")){
                        launchHomeActivity();
                        sessionmanagerObj.set_LoggedIn(true);
                    }else{
                        errortext.setText("* Contact number and password is not matched.");
                    }
                }else{
                    errortext.setText("* Please provide contact number and password to continue.");
                }
            }
        });
    }

    public void launchHomeActivity(){
        Intent i = new Intent(activityName,HomeActivity.class);
        startActivity(i);
        finish();
    }
}

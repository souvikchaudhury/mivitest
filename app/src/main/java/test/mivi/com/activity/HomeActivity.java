package test.mivi.com.activity;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import test.mivi.com.R;
import test.mivi.com.adapter.ServiceAdapter;
import test.mivi.com.model.AccountsModel;
import test.mivi.com.model.ServicesModel;

import static test.mivi.com.helper.UtilClass.collectedData;
import static test.mivi.com.helper.UtilClass.getCustomerInfo;
import static test.mivi.com.helper.UtilClass.getservicesInfo;

public class HomeActivity extends AppCompatActivity {

    Context activityName;
    ArrayList<ServicesModel> servicesMasterArray;
    AccountsModel accountInfoObj;

    @BindView(R.id.serviceRecycler)RecyclerView serviceRecycler;

    @BindView(R.id.customerName)TextView customerName;
    @BindView(R.id.customer_email)TextView customer_email;
    @BindView(R.id.contact_no)TextView contact_no;

    @BindView(R.id.payment_type)TextView payment_type;
    @BindView(R.id.unbill_charge)TextView unbill_charge;
    @BindView(R.id.nextBillDate)TextView nextBillDate;
    @BindView(R.id.dob)TextView dob;
    @BindView(R.id.is_email_verify)TextView is_email_verify;
    @BindView(R.id.subcription_stat)TextView subcription_stat;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        activityName = HomeActivity.this;
        JSONObject collectedJsonData = collectedData(activityName);

        accountInfoObj = getCustomerInfo(collectedJsonData);
        servicesMasterArray = getservicesInfo(collectedJsonData);

        customerName.setText(accountInfoObj.getNameTitle()+" "+accountInfoObj.getFirst_name()+" "+accountInfoObj.getLast_name());
        customer_email.setText("Email id: "+accountInfoObj.getEmail_address());
        contact_no.setText("Contact No. "+accountInfoObj.getContact_number());

        payment_type.setText(accountInfoObj.getPayment_type());
        unbill_charge.setText(accountInfoObj.getUnbilled_charges());
        nextBillDate.setText(accountInfoObj.getNext_billing_date());
        dob.setText(accountInfoObj.getDate_of_birth());
        is_email_verify.setText(""+accountInfoObj.isEmail_address_verified());
        subcription_stat.setText(""+accountInfoObj.isEmail_subscription_status());


        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        serviceRecycler.setLayoutManager(manager);
        //serviceRecycler.setHasFixedSize(true);
        ServiceAdapter parentAdapter = new ServiceAdapter(this, servicesMasterArray);
        serviceRecycler.setAdapter(parentAdapter);


    }
}

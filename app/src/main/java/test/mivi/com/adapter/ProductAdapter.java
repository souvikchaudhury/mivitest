package test.mivi.com.adapter;

import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;


import butterknife.ButterKnife;
import test.mivi.com.R;
import test.mivi.com.model.ProductModel;

/**
 * Created by Souvik Chaudhury on 22-06-2018.
 */

public class ProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<ProductModel> childData;

    public ProductAdapter(ArrayList<ProductModel> childData) {
        this.childData = childData;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView productid,productName,productIncludedData,productIncludedCredit;
        TextView product_international_talk,product_unlimited_text,product_unlimited_talk;
        TextView product_unlimited_international_text,product_unlimited_international_talk,product_price;

        public ViewHolder(View itemView) {
            super(itemView);
            productid = (TextView) itemView.findViewById(R.id.productid);
            productName = (TextView) itemView.findViewById(R.id.productName);
            productIncludedData = (TextView) itemView.findViewById(R.id.productIncludedData);
            productIncludedCredit = (TextView) itemView.findViewById(R.id.productIncludedCredit);

            product_international_talk = (TextView) itemView.findViewById(R.id.product_international_talk);
            product_unlimited_text = (TextView) itemView.findViewById(R.id.product_unlimited_text);
            product_unlimited_talk = (TextView) itemView.findViewById(R.id.product_unlimited_talk);

            product_unlimited_international_text = (TextView) itemView.findViewById(R.id.product_unlimited_international_text);
            product_unlimited_international_talk = (TextView) itemView.findViewById(R.id.product_unlimited_international_talk);
            product_price = (TextView) itemView.findViewById(R.id.product_price);

        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_layout, parent, false);

        ProductAdapter.ViewHolder cavh = new ProductAdapter.ViewHolder(itemLayoutView);
        return cavh;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final ViewHolder vh = (ViewHolder) holder;

        ProductModel c = childData.get(position);

        vh.productid.setText(c.getProduct_id());
        vh.productName.setText(c.getProduct_name());
        vh.productIncludedData.setText(c.getIncluded_data());
        vh.productIncludedCredit.setText(c.getIncluded_credit());
        vh.product_international_talk.setText(c.getIncluded_international_talk());
        vh.product_unlimited_text.setText(c.getUnlimited_text());
        vh.product_unlimited_talk.setText(c.getUnlimited_talk());
        vh.product_unlimited_international_talk.setText(""+c.isUnlimited_international_talk());
        vh.product_unlimited_international_text.setText(""+c.isUnlimited_international_text());
        vh.product_price.setText(c.getProduct_price());
    }

    @Override
    public int getItemCount() {
        return childData.size();
    }
}
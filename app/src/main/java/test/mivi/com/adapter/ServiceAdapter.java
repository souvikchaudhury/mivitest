package test.mivi.com.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import test.mivi.com.R;
import test.mivi.com.helper.NestedRecyclerLinearLayoutManager;
import test.mivi.com.model.ServicesModel;
import test.mivi.com.model.SubcriptionsModel;

/**
 * Created by Souvik Chaudhury on 22-06-2018.
 */

public class ServiceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<ServicesModel> parentChildData;
    Context ctx;

    public ServiceAdapter(Context ctx, ArrayList<ServicesModel> parentChildData) {
        this.ctx = ctx;
        this.parentChildData = parentChildData;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        RecyclerView subcriptionRecycler;
        TextView serviceid,creditinfo,creditExpiryDate,nossubscriptionAvailable;

        public ViewHolder(View itemView) {
            super(itemView);

            serviceid = (TextView) itemView.findViewById(R.id.serviceid);
            creditinfo = (TextView) itemView.findViewById(R.id.creditinfo);
            creditExpiryDate = (TextView) itemView.findViewById(R.id.creditExpiryDate);
            nossubscriptionAvailable = (TextView) itemView.findViewById(R.id.nossubscriptionAvailable);
            subcriptionRecycler = (RecyclerView) itemView.findViewById(R.id.subcriptionRecycler);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.service_layout, parent, false);
        ServiceAdapter.ViewHolder pavh = new ServiceAdapter.ViewHolder(itemLayoutView);
        return pavh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ServiceAdapter.ViewHolder vh = (ServiceAdapter.ViewHolder) holder;

        ServicesModel p = parentChildData.get(position);

        vh.serviceid.setText("# "+p.getServiceid());
        vh.creditinfo.setText(p.getCredit());
        vh.creditExpiryDate.setText(p.getCredit_expiry());

        if(p.getSubcriptionsModelobj().size()<=0){
            vh.nossubscriptionAvailable.setVisibility(View.VISIBLE);
        }else{
            vh.nossubscriptionAvailable.setVisibility(View.GONE);
        }

        initChildLayoutManager(vh.subcriptionRecycler, p.getSubcriptionsModelobj());
    }

    private void initChildLayoutManager(RecyclerView rv_child, ArrayList<SubcriptionsModel> childmData) {
        rv_child.setLayoutManager(new NestedRecyclerLinearLayoutManager(ctx));
        SubcriptionAdapter childAdapter = new SubcriptionAdapter(childmData);
        rv_child.setAdapter(childAdapter);
    }

    @Override
    public int getItemCount() {

        return parentChildData.size();
    }
}

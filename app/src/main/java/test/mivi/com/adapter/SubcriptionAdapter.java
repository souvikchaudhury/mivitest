package test.mivi.com.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.ButterKnife;
import test.mivi.com.R;
import test.mivi.com.helper.NestedRecyclerLinearLayoutManager;
import test.mivi.com.model.ProductModel;
import test.mivi.com.model.SubcriptionsModel;

/**
 * Created by Souvik Chaudhury on 22-06-2018.
 */

public class SubcriptionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<SubcriptionsModel> parentChildData;
    Context ctx;

    public SubcriptionAdapter(ArrayList<SubcriptionsModel> childData) {
        this.parentChildData = childData;
    }

    public SubcriptionAdapter(Context ctx, ArrayList<SubcriptionsModel> parentChildData) {
        this.ctx = ctx;
        this.parentChildData = parentChildData;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        RecyclerView productRecycler;
        TextView subscriptionid,subs_included_data_balance,subs_included_credit_balance,noproductAvailable;
        TextView subs_included_rollover_credit_balance,subs_included_rollover_data_balance;
        TextView subs_included_international_talk_balance,subs_expiry_date,autorenewal,primarySubscription;

        public ViewHolder(View itemView) {
            super(itemView);

            subscriptionid = (TextView) itemView.findViewById(R.id.subscriptionid);
            subs_included_data_balance = (TextView) itemView.findViewById(R.id.subs_included_data_balance);
            subs_included_credit_balance = (TextView) itemView.findViewById(R.id.subs_included_credit_balance);
            subs_included_rollover_credit_balance = (TextView) itemView.findViewById(R.id.subs_included_rollover_credit_balance);
            subs_included_rollover_data_balance = (TextView) itemView.findViewById(R.id.subs_included_rollover_data_balance);
            subs_included_international_talk_balance = (TextView) itemView.findViewById(R.id.subs_included_international_talk_balance);
            subs_expiry_date = (TextView) itemView.findViewById(R.id.subs_expiry_date);
            autorenewal = (TextView) itemView.findViewById(R.id.autorenewal);
            noproductAvailable = (TextView) itemView.findViewById(R.id.noproductAvailable);
            primarySubscription = (TextView) itemView.findViewById(R.id.primarySubscription);

            productRecycler = (RecyclerView) itemView.findViewById(R.id.productRecycler);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.subscription_layout, parent, false);
        SubcriptionAdapter.ViewHolder pavh = new SubcriptionAdapter.ViewHolder(itemLayoutView);
        return pavh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder vh = (ViewHolder) holder;
        SubcriptionsModel p = parentChildData.get(position);

        vh.subscriptionid.setText("# "+p.getSubcription_id());
        vh.subs_included_data_balance.setText(p.getIncluded_data_balance());
        vh.subs_included_credit_balance.setText(p.getIncluded_credit_balance());
        vh.subs_included_rollover_credit_balance.setText(p.getIncluded_rollover_credit_balance());
        vh.subs_included_rollover_data_balance.setText(p.getIncluded_rollover_data_balance());
        vh.subs_included_international_talk_balance.setText(p.getIncluded_international_talk_balance());

        vh.subs_expiry_date.setText(p.getExpiry_date());
        vh.autorenewal.setText(""+p.isAuto_renewal());
        vh.primarySubscription.setText(""+p.isPrimary_subscription());

        if(p.getProductModelobj().size()<=0){
            vh.noproductAvailable.setVisibility(View.VISIBLE);
        }else{
            vh.noproductAvailable.setVisibility(View.GONE);
        }

        initChildLayoutManager(vh.productRecycler, p.getProductModelobj());
    }

    private void initChildLayoutManager(RecyclerView rv_child, ArrayList<ProductModel> childData) {
        rv_child.setLayoutManager(new NestedRecyclerLinearLayoutManager(ctx));
        ProductAdapter childAdapter = new ProductAdapter(childData);
        rv_child.setAdapter(childAdapter);
    }

    @Override
    public int getItemCount() {
        return parentChildData.size();
    }
}

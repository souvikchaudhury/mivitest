**To Login in this system please use this following credentials**

Contact No : 0404000000

Password: 12345


## Technical Test Question Paper

Using our public collection.json as a source of customer data we want you to create an app that consumes data from the collection and does the following:

1. Create an initial login screen that allows a customer to login (we don't want you to actually create an authentication layer, just validate the MSN credentials in the collection)
2. Display a simple welcome to mivi splash page
3. Transition through to a screen that shows the customer the following information:

* information about their plan, also known as a subscription, remaining data balance of their subscription
* information about the product that is related to the subscription, including the name and price of the product
* any other information you think may be relevant to the customer
